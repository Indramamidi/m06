# M06 Chat Example

Starting on a simple chat demo using node.js, express, and socket.io.

https://bitbucket.org/professorcase/m06

## Requirements

- Install Git version control system
- Install Node.js, an open-source, cross-platform JavaScript run-time environment for executing JavaScript code on the server. 
- Install Visual Studio Code for editing. 

## Get the Code

1. Log in to BitBucket. 

2. Fork this repo from this BitBucket cloud repository into your own cloud repository.

3. Clone it from your cloud repository down to your local machine. 

## Run it

Open a command window in your c:\44563\m06 folder.

Run **npm install** to install all the dependencies in the package.json file.

Run **node server.js** or **node server** to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node server.js
```

Point your browser to `http://localhost:8081`.

## Reference

Based on http://javabeginnerstutorial.com/javascript-2/create-simple-chat-application-using-node-js-express-js-socket-io/

## Continue the app 

https://bitbucket.org/professorcase/w06
